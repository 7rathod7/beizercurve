#include <iostream>

#define GLEW_STATIC
#define GLM_ENABLE_EXPERIMENTAL
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Shader.h"
#include "Camera.h"
#include <SOIL\SOIL.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\ext.hpp>
#include <glm/gtx/string_cast.hpp>
#include <vector>
#include <tuple>

using namespace std;

vector<tuple<float, float, float>> points;
bool check = false;

// Properties
GLuint screenWidth = 800, screenHeight = 600;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void Do_Movement();

// Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLuint lineVAO, lineVBO;
glm::mat4 view;
glm::mat4 projection;
glm::mat4 model;


int main() {
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "shader", nullptr, nullptr);

	if (window == nullptr) {
		cout << "failed to create glfw window\n";
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK) {
		cout << "failed to initialise glew\n";
		return -1;
	}

	const GLubyte *renderer = glGetString(GL_RENDERER);
	const GLubyte *version = glGetString(GL_VERSION);
	GLint nrVertexAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrVertexAttributes);
	cout << "Maximum number of vertex attributes are supported -> " << nrVertexAttributes << "\n";

	cout << "renderer -> " << renderer << "\n";
	cout << "version -> " << version << "\n";

	glViewport(0, 0, 800, 600);

	float vertices1[] = {0.0f, 0.0f, 0.0f, 1.0f, 0.0f, -5.0f, 5.0f, 0.0f, -5.0f, 5.0f, 0.0f, 0.0f};

	GLuint VAO1, VBO1;
	glGenBuffers(1, &VBO1);
	glGenVertexArrays(1, &VAO1);
	glBindVertexArray(VAO1);
	glBindBuffer(GL_ARRAY_BUFFER, VBO1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1), vertices1, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glGenVertexArrays(1, &lineVAO);
	glGenBuffers(1, &lineVBO);
	glBindVertexArray(lineVAO);
	glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	Shader shader("vertex.glsl", "fragment.glsl");

	points.push_back(make_tuple(0.0f, 0.0f, 0.0f));
	points.push_back(make_tuple(1.0f, 0.0f, -5.0f));
	points.push_back(make_tuple(5.0f, 0.0f, -5.0f));
	points.push_back(make_tuple(5.0f, 0.0f, 0.0f));

	glm::vec3 color = glm::vec3(1.0f, 0.0f, 1.0f);
	float data[75];
	while (!glfwWindowShouldClose(window)) {
		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// Check and call events
		glfwPollEvents();
		Do_Movement();
		if (check) 
		{
			GLfloat a, b, c;
			cout << "\nEnter the input values there : ";
			for (int i = 0; i < 3; i++)
			{
				cin >> a >> b >> c;
				points.push_back(make_tuple(a, b, c));
			}
			check = false;
		}

		// Clear the colorbuffer
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Set uniforms
		view = camera.GetViewMatrix();
		projection = glm::perspective(camera.Zoom, (float)screenWidth / (float)screenHeight, 0.1f, 100.0f);
		shader.Use();
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniform3fv(glGetUniformLocation(shader.Program, "vertexColor"), 1, &color[0]);
		model = glm::mat4(1.0f);
		glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
		int pointCount = 0;
		while (pointCount < points.size())
		{
			int count = 0;
			for (int j = 0; j < 12; j++) 
			{
				vertices1[j++] = get<0>(points[pointCount]);
				vertices1[j++] = get<1>(points[pointCount]);
				vertices1[j] = get<2>(points[pointCount]);
				pointCount++;
			}
			for (float a = 0; a < 1.0f; a += 0.04f) {
				//drawline(points, a, glm::vec4(0.0f, 0.5f, 0.3f, 1.0f), shader);

				data[count++] = (1 - a)*(1 - a)*(1 - a)*vertices1[0] + 3 * a*(1 - a)*(1 - a)*vertices1[3] + 3 * a*a*(1 - a)*vertices1[6] + a * a*a*vertices1[9];
				data[count++] = (1 - a)*(1 - a)*(1 - a)*vertices1[1] + 3 * a*(1 - a)*(1 - a)*vertices1[4] + 3 * a*a*(1 - a)*vertices1[7] + a * a*a*vertices1[10];
				data[count++] = (1 - a)*(1 - a)*(1 - a)*vertices1[2] + 3 * a*(1 - a)*(1 - a)*vertices1[5] + 3 * a*a*(1 - a)*vertices1[8] + a * a*a*vertices1[11];
				//camera.Position.x = data[count - 3];
				//camera.Position.y = data[count - 2];
				//camera.Position.z = data[count - 1];
				//float vertices3[] = {camera.Position.x, camera.Position.y, camera.Position.z};
				///*int n;
				//cin >> n;*/
				//glBindVertexArray(lineVAO);
				//glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
				//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices3), vertices3, GL_DYNAMIC_DRAW);
				//glPointSize(2);
				//glDrawArrays(GL_POINTS, 0, 25);
			}

			glBindVertexArray(lineVAO);
			glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_DYNAMIC_DRAW);
			glPointSize(2);
			glDrawArrays(GL_POINTS, 0, 25);
			if (pointCount == points.size()) break;
			else pointCount -= 1;
		}

		float data1[100];
		int count = 0;
		for (auto g : points) 
		{
			data1[count++] = get<0>(g);
			data1[count++] = get<1>(g);
			data1[count++] = get<2>(g);
		}
		glBindVertexArray(lineVAO);
		glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(data1), data1, GL_DYNAMIC_DRAW);
		glPointSize(25);
		glDrawArrays(GL_POINTS, 0, points.size());
		glfwSwapBuffers(window);
	}
	glDeleteVertexArrays(1, &lineVAO);
	glDeleteBuffers(1, &lineVBO);

	glfwTerminate();
	return 0;
}

// Moves/alters the camera positions based on user input
void Do_Movement()
{
	// Camera controls
	if (keys[GLFW_KEY_W])
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (keys[GLFW_KEY_S])
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (keys[GLFW_KEY_A])
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (keys[GLFW_KEY_D])
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	
	if (key == GLFW_KEY_I && action == GLFW_PRESS) 
	{
		check = true;
		//cout << glm::to_string() << "\n";
	}

	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos;

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}